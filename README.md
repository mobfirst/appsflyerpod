# AppsFlyerPod

[![CI Status](https://img.shields.io/travis/Diego Merks/AppsFlyerPod.svg?style=flat)](https://travis-ci.org/Diego Merks/AppsFlyerPod)
[![Version](https://img.shields.io/cocoapods/v/AppsFlyerPod.svg?style=flat)](https://cocoapods.org/pods/AppsFlyerPod)
[![License](https://img.shields.io/cocoapods/l/AppsFlyerPod.svg?style=flat)](https://cocoapods.org/pods/AppsFlyerPod)
[![Platform](https://img.shields.io/cocoapods/p/AppsFlyerPod.svg?style=flat)](https://cocoapods.org/pods/AppsFlyerPod)

## Example

```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[AppsFlyerEventTracker getInstance] setParams:@"b3xKbWm5QgRRmEjago4s8M" appleAppID:@"1435214477" andDebugMode:NO];
    return YES;
}
```

## Requirements

## Installation

AppsFlyerPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AppsFlyerPod'
```

## Author

Diego Merks, merks@mobfirst.com

## License

AppsFlyerPod is available under the MIT license. See the LICENSE file for more info.
